﻿namespace fortnite
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.factInput = new System.Windows.Forms.TextBox();
            this.factBtn = new System.Windows.Forms.Button();
            this.factSonuc = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.barBtn = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.fanInput = new System.Windows.Forms.TextBox();
            this.fanBtn = new System.Windows.Forms.Button();
            this.fanSonuc = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(55, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Faktoriyel hesaplama";
            // 
            // factInput
            // 
            this.factInput.Location = new System.Drawing.Point(61, 69);
            this.factInput.Name = "factInput";
            this.factInput.Size = new System.Drawing.Size(100, 20);
            this.factInput.TabIndex = 1;
            // 
            // factBtn
            // 
            this.factBtn.Location = new System.Drawing.Point(179, 69);
            this.factBtn.Name = "factBtn";
            this.factBtn.Size = new System.Drawing.Size(75, 23);
            this.factBtn.TabIndex = 2;
            this.factBtn.Text = "Hesapla";
            this.factBtn.UseVisualStyleBackColor = true;
            this.factBtn.Click += new System.EventHandler(this.factBtn_Click);
            // 
            // factSonuc
            // 
            this.factSonuc.AutoSize = true;
            this.factSonuc.Location = new System.Drawing.Point(100, 101);
            this.factSonuc.Name = "factSonuc";
            this.factSonuc.Size = new System.Drawing.Size(41, 13);
            this.factSonuc.TabIndex = 3;
            this.factSonuc.Text = "Sonuç:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(374, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(108, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Timer ve ProgressBar";
            // 
            // barBtn
            // 
            this.barBtn.Location = new System.Drawing.Point(406, 68);
            this.barBtn.Name = "barBtn";
            this.barBtn.Size = new System.Drawing.Size(109, 23);
            this.barBtn.TabIndex = 5;
            this.barBtn.Text = "TURAN KUR";
            this.barBtn.UseVisualStyleBackColor = true;
            this.barBtn.Click += new System.EventHandler(this.barBtn_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(377, 101);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(301, 23);
            this.progressBar1.TabIndex = 6;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label3.Location = new System.Drawing.Point(174, 269);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(413, 25);
            this.label3.TabIndex = 7;
            this.label3.Text = "UYARI: BURADAN AŞAĞISI FANTAZİ!!!!!!!";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(58, 320);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(192, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Ondalık tabanı ikilik tabana dönüştürme";
            // 
            // fanInput
            // 
            this.fanInput.Location = new System.Drawing.Point(103, 380);
            this.fanInput.Name = "fanInput";
            this.fanInput.Size = new System.Drawing.Size(100, 20);
            this.fanInput.TabIndex = 9;
            // 
            // fanBtn
            // 
            this.fanBtn.Location = new System.Drawing.Point(251, 380);
            this.fanBtn.Name = "fanBtn";
            this.fanBtn.Size = new System.Drawing.Size(106, 23);
            this.fanBtn.TabIndex = 10;
            this.fanBtn.Text = "çoştur beni askm";
            this.fanBtn.UseVisualStyleBackColor = true;
            this.fanBtn.Click += new System.EventHandler(this.fanBtn_Click);
            // 
            // fanSonuc
            // 
            this.fanSonuc.AutoSize = true;
            this.fanSonuc.Location = new System.Drawing.Point(446, 380);
            this.fanSonuc.Name = "fanSonuc";
            this.fanSonuc.Size = new System.Drawing.Size(41, 13);
            this.fanSonuc.TabIndex = 11;
            this.fanSonuc.Text = "Sonuç:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.fanSonuc);
            this.Controls.Add(this.fanBtn);
            this.Controls.Add(this.fanInput);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.barBtn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.factSonuc);
            this.Controls.Add(this.factBtn);
            this.Controls.Add(this.factInput);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox factInput;
        private System.Windows.Forms.Button factBtn;
        private System.Windows.Forms.Label factSonuc;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button barBtn;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox fanInput;
        private System.Windows.Forms.Button fanBtn;
        private System.Windows.Forms.Label fanSonuc;
    }
}

